//Created by Christopher ROQUE

#include <stdio.h>
#include <stdlib.h>
#include "AES-128.h"



int main(int argc, char **argv)
{
        uint8_t* plaintext = "Two One Nine Two";
        uint8_t* key = "Thats my Kung Fu";
        uint8_t ciphertext[16];

        //Uncomment if you want DEBUG Version
        /*
        uint8_t state[STATE_ROW_SIZE][STATE_ROW_SIZE];
        uint8_t masterkey[STATE_ROW_SIZE][STATE_ROW_SIZE];
        uint8_t roundkeys[ROUND_COUNT+1][STATE_ROW_SIZE][STATE_ROW_SIZE];
        uint8_t round_key[STATE_ROW_SIZE][STATE_ROW_SIZE];
        */

        int i = 0;
        //int round = 0;

        //Uncomment if you want DEBUG Version
        /*
        printf("##### Plaintext to State Matrix #####\n");
        MessageToState(state, plaintext);
        print_state(state);
        printf("\n");
        printf("##### Key to State Matrix #####\n");
        MessageToState(masterkey, key);
        print_state(masterkey);
        printf("\n");
        printf("##### Key Expansion #####\n");
        KeyGen(roundkeys, masterkey);
        print_round_keys(roundkeys);
        printf("\n##### Round 0 #####\n");
        GetRoundKey(round_key,roundkeys, round);
        printf("RoundKey 0 : \n");
        print_state(round_key);
        printf("\n XOR \n");
        printf("\nRelated State : \n");
        print_state(state);
        printf("\n = \n\n");
        AddRoundKey(state,round_key);
        print_state(state);
        printf("\n##### Rounds 1 to 9 #####\n");
        printf("\n\n");
        */
        printf("\n##### Ciphertext #####\n");
        AESEncrypt(ciphertext,plaintext,key);
        for(i=0; i<16; i++)
        {
                printf("%02X ", ciphertext[i]);
        }

        printf("\n");
        return 0;
}