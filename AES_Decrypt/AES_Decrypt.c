//Created by Christopher ROQUE

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "AES-128.h"

uint8_t SBOX[256] = {
        0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
        0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
        0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
        0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
        0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
        0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
        0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
        0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
        0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
        0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
        0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
        0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
        0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
        0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
        0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
        0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
};

uint8_t inv_SBOX[256] = {
        0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,
        0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
        0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
        0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
        0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
        0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
        0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
        0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
        0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
        0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
        0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
        0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
        0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
        0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
        0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
        0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D
};

uint8_t FIXEDMATRIX[STATE_ROW_SIZE][STATE_ROW_SIZE] = {
        {0x02, 0x03, 0x01, 0x01},
        {0x01, 0x02, 0x03, 0x01},
        {0x01, 0x01, 0x02, 0x03},
        {0x03, 0x01, 0x01, 0x02}
};

uint8_t inv_FIXEDMATRIX[STATE_ROW_SIZE][STATE_ROW_SIZE] = {
        {0x0e, 0x0b, 0x0d, 0x09},
        {0x09, 0x0e, 0x0b, 0x0d},
        {0x0d, 0x09, 0x0e, 0x0b},
        {0x0b, 0x0d, 0x09, 0x0e}
};

/* for 128-bit blocks, Rijndael never uses more than 10 rcon values */

uint8_t RCON[10] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36};

void MessageToState(uint8_t state [][STATE_ROW_SIZE], uint8_t message [])
{
        int i,j;

        for (i=0; i<STATE_ROW_SIZE; i++)
        {
                for(j=0; j<STATE_ROW_SIZE; j++)
                {
                        state[j][i] = message[i*STATE_ROW_SIZE+j];
                }
        }

        return;
}

void StateToMessage(uint8_t message [],uint8_t state[][STATE_ROW_SIZE])
{
        int i,j;

        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                for(j=0; j<STATE_ROW_SIZE; j++)
                {
                        message[i*STATE_ROW_SIZE+j] = state[j][i];
                }
        }

        return;
}

void KeyGen(uint8_t keys [][STATE_ROW_SIZE][STATE_ROW_SIZE], uint8_t master_key [][STATE_ROW_SIZE])
{
        int i,j,k;

        //keys [][STATE_ROW_SIZE][STATE_ROW_SIZE] : X square Matrices.

        //first add the Private Key
        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                for(j=0; j<STATE_ROW_SIZE; j++)
                {
                        keys[0][i][j] = master_key[i][j];
                }
        }

        //Key Expansion : generation of 10 round keys -> total "keys" size = 11*4*4
        //k : index of key generated related to the actual round.

        for (k=1; k<ROUND_COUNT+1; k++)
        {

                //##### First column of a matrix filling#####
                uint8_t first_column[STATE_ROW_SIZE];

                //Step 1 : RotWord
        
                first_column[0] = keys[k-1][1][3]; //[1][3] : byte on line 1 and column 3 
                first_column[1] = keys[k-1][2][3];
                first_column[2] = keys[k-1][3][3];
                first_column[3] = keys[k-1][0][3];

                //Step 2 : SubBytes
                
                for(i=0; i<STATE_ROW_SIZE; i++)
                {
                        first_column[i] = SBOX[first_column[i]];
                }
                
                //Step 3 : XOR with Wi-4 and Rcon value
                keys[k][0][0] = keys[k-1][0][0] ^ first_column[0] ^ RCON[k-1];

                for(i=1; i<STATE_ROW_SIZE; i++)
                {
                        keys[k][i][0] = keys[k-1][i][0] ^ first_column[i];
                }

                //##### Filling othen columns #####
        
                for(i=0; i<STATE_ROW_SIZE; i++)
                {
                        for(j=1; j<STATE_ROW_SIZE; j++)
                        {
                                keys[k][i][j] = keys[k-1][i][j] ^ keys[k][i][j-1];
                        }       
                }
        
        }
    
        return ;

}

void AddRoundKey( uint8_t tableau_1[][STATE_ROW_SIZE], uint8_t tableau_2[][STATE_ROW_SIZE])
{
        int i,j;

        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                for(j=0; j<STATE_ROW_SIZE; j++)
                {
                        //XOR between state at round X and roundkey X
                        tableau_1[i][j] = tableau_1[i][j] ^ tableau_2[i][j]; 
                }
        }
        
        return;
}

void GetRoundKey(uint8_t round_key [][STATE_ROW_SIZE], uint8_t keys [][STATE_ROW_SIZE][STATE_ROW_SIZE], int round)
{
        int i,j;

        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                for(j=0; j<STATE_ROW_SIZE; j++)
                {
                        round_key[i][j] = keys[round][i][j];
                }
        }

        return;
}

void SubBytes(uint8_t state[][STATE_ROW_SIZE])
{
        int i,j;

        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                for(j=0;j<STATE_ROW_SIZE;j++)
                {
                        state[i][j] = SBOX[state[i][j]];
                }
        }

        return;
}

void inv_SubBytes(uint8_t state[][STATE_ROW_SIZE])
{
        int i,j;

        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                for(j=0;j<STATE_ROW_SIZE;j++)
                {
                        state[i][j] = inv_SBOX[state[i][j]];
                }
        }

        return;
}

void ShiftRows(uint8_t state[][STATE_ROW_SIZE])
{
        int i,j;
        int shift;
        uint8_t temp[STATE_ROW_SIZE];

        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                for(shift=0; shift<i; shift++)
                {
                        temp[i]=state[i][0];
                        for(j=0; j<STATE_ROW_SIZE; j++)
                        {
                                state[i][j] = state[i][j+1];
                        }
                        state[i][STATE_ROW_SIZE-1] = temp[i];

                }

        }


        return;
}

void inv_ShiftRows(uint8_t state[][STATE_ROW_SIZE])
{
        int i,j;
        int shift;
        uint8_t temp[STATE_ROW_SIZE];

        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                for(shift=0; shift<i; shift++)
                {
                        temp[i]=state[i][STATE_ROW_SIZE-1];
                        for(j=STATE_ROW_SIZE-1; j>0; j--)
                        {
                                state[i][j] = state[i][j-1];
                        }
                        state[i][0] = temp[i];

                }

        }


        return;
}

void MixColumns(uint8_t state [][STATE_ROW_SIZE])
{

        int i,j,k;
        uint8_t buffer;
        uint8_t temp[STATE_ROW_SIZE][STATE_ROW_SIZE];

        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                for(j=0; j<STATE_ROW_SIZE; j++)
                {
                        temp[i][j] = state[i][j];
                }
        }

        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                
                for(j=0; j<STATE_ROW_SIZE; j++)
                {
                        buffer=0x00;
                        for(k=0; k<STATE_ROW_SIZE; k++)
                        {
                                buffer = buffer ^ gmul(FIXEDMATRIX[i][k], temp[k][j]);
                        }
                        state[i][j] = buffer;

                }
                
        }
        return;


}

void inv_MixColumns(uint8_t state [][STATE_ROW_SIZE])
{

        int i,j,k;
        uint8_t buffer;
        uint8_t temp[STATE_ROW_SIZE][STATE_ROW_SIZE];

        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                for(j=0; j<STATE_ROW_SIZE; j++)
                {
                        temp[i][j] = state[i][j];
                }
        }

        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                
                for(j=0; j<STATE_ROW_SIZE; j++)
                {
                        buffer=0x00;
                        for(k=0; k<STATE_ROW_SIZE; k++)
                        {
                                buffer = buffer ^ gmul(inv_FIXEDMATRIX[i][k], temp[k][j]);
                        }
                        state[i][j] = buffer;

                }
                
        }
        return;


}

uint8_t gmul(uint8_t a, uint8_t b)
{
        int i;
        uint8_t product=0, temp=b;
        
        for(i=0; i<8; i++)
        {
                if(a & ((uint8_t)1<<i)) {product ^= temp;}
                temp = (temp & ((uint8_t)1 << 7) ? (temp<<1) ^ POLY_GF2_8 : temp<<1);
        }
        return product;
}


void print_state(uint8_t state[STATE_ROW_SIZE][STATE_ROW_SIZE])
{
        int i,j;
        
        for(i=0; i<STATE_ROW_SIZE; i++)
        {
                for(j=0; j<STATE_ROW_SIZE; j++)
                {
                        printf("%02X ", state[i][j]);
                }

                printf("\n");
        }

        return;
}

void print_round_keys(uint8_t roundkeys[ROUND_COUNT+1][STATE_ROW_SIZE][STATE_ROW_SIZE])
{
        int i,j,k;
        for(k=0; k<ROUND_COUNT+1; k++)
        {
                printf("RoundKey n°%i : ",k);

                for(i=0; i<STATE_ROW_SIZE; i++)
                {       
                        for(j=0; j<STATE_ROW_SIZE; j++)
                        {
                                printf("%02X ", roundkeys[k][j][i]);
                        }
                }

                printf("\n");
        }

        return;

}

void AESEncrypt(uint8_t* ciphertext, uint8_t* plaintext , uint8_t* key)
{
        int round;
        uint8_t state[STATE_ROW_SIZE][STATE_ROW_SIZE];
        uint8_t masterkey[STATE_ROW_SIZE][STATE_ROW_SIZE];
        uint8_t roundkeys[ROUND_COUNT+1][STATE_ROW_SIZE][STATE_ROW_SIZE];
        uint8_t round_key[STATE_ROW_SIZE][STATE_ROW_SIZE];

        //initilisation
        MessageToState(state, plaintext);
        MessageToState(masterkey, key);
        KeyGen(roundkeys, masterkey);

        //round 0
        AddRoundKey(state, masterkey);

        //round 1 to 9
        for(round=1; round<ROUND_COUNT; round++)
        {
                SubBytes(state);
                ShiftRows(state);
                MixColumns(state);
                GetRoundKey(round_key, roundkeys, round);
                AddRoundKey(state,round_key);
        }

        //round 10
        SubBytes(state);
        ShiftRows(state);
        GetRoundKey(round_key,roundkeys,ROUND_COUNT);
        AddRoundKey(state,round_key);
        StateToMessage(ciphertext, state);
        
}

void AESDecrypt(uint8_t* ciphertext, uint8_t* plaintext , uint8_t* key)
{
        int round;
        uint8_t state[STATE_ROW_SIZE][STATE_ROW_SIZE];
        uint8_t masterkey[STATE_ROW_SIZE][STATE_ROW_SIZE];
        uint8_t roundkeys[ROUND_COUNT+1][STATE_ROW_SIZE][STATE_ROW_SIZE];
        uint8_t round_key[STATE_ROW_SIZE][STATE_ROW_SIZE];

        //initilisation
        MessageToState(state, ciphertext);
        MessageToState(masterkey, key);
        KeyGen(roundkeys, masterkey);

        GetRoundKey(round_key,roundkeys,ROUND_COUNT);
        AddRoundKey(state,round_key);
        inv_ShiftRows(state);
        inv_SubBytes(state);
        //print_state(state);


        //round 1 to 9
        for(round=ROUND_COUNT-1; round>0; round--)
        {
                GetRoundKey(round_key, roundkeys, round);
                AddRoundKey(state,round_key);
                inv_MixColumns(state);
                inv_ShiftRows(state);
                inv_SubBytes(state);
                
                
        }
        //round 0
        AddRoundKey(state, masterkey);
        StateToMessage(plaintext,state);
        
}

