//Created by Christopher ROQUE

#include <stdint.h>

#ifndef AES_H
#define AES_H

#define DATA_SIZE 16
#define STATE_ROW_SIZE 4
#define ROUND_COUNT 10
#define POLY_GF2_8 (uint8_t)0x1B


void AESEncrypt(uint8_t* ciphertext, uint8_t* plaintext , uint8_t* key);
void AddRoundKey( uint8_t tableau_1 [][STATE_ROW_SIZE], uint8_t tableau_2 [][STATE_ROW_SIZE]);
void SubBytes(uint8_t state[][STATE_ROW_SIZE]);
void ShiftRows(uint8_t state[][STATE_ROW_SIZE]);
void MixColumns(uint8_t state [][STATE_ROW_SIZE]);
void KeyGen(uint8_t keys [][STATE_ROW_SIZE][STATE_ROW_SIZE], uint8_t master_key [][STATE_ROW_SIZE]);
void ColumnFill(uint8_t keys[][STATE_ROW_SIZE][STATE_ROW_SIZE] , int round);
void OtherColumnsFill(uint8_t keys[][STATE_ROW_SIZE][STATE_ROW_SIZE], int round);
void GetRoundKey(uint8_t round_key [][STATE_ROW_SIZE], uint8_t keys [][STATE_ROW_SIZE][STATE_ROW_SIZE], int round);
void MessageToState(uint8_t state [][STATE_ROW_SIZE], uint8_t message []);
void StateToMessage(uint8_t message [],uint8_t state [][STATE_ROW_SIZE]);
void MCMatrixColumnProduct(uint8_t colonne []);
uint8_t gmul(uint8_t a, uint8_t b);


#endif